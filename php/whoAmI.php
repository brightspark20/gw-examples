<?php
require("_inc.php");
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/site.css" />
</head>
<body>
    <div class="container body-content">
            
        <div>
            <a href="index.php">Cleanup tool</a> | 
            <a href="whoAmI.php">Who am i?</a>
        </div>

        <div class="jumbotron">
            <h1>WhoAmI?</h1>
        </div>

        <h2>Find out from</h2>

        <ul class="banks">
            <li data-provider="1">Swedbank</li>
            <li data-provider="2">SEB</li>
            <li data-provider="7">LHV</li>
            <li data-provider="6">Coop Pank</li>
            <li data-provider="9">Luminor</li>
        </ul>

        <h2>Client id</h2>
        <input type="text" name="ClientId" id="ClientId" class="form-control" value="<?= $clientId ?>" />

        <h2>Salt</h2>
        <input type="text" name="Salt" id="Salt" class="form-control" value="<?= $clientSalt ?>" />

        <br/>
        <br/>

        <a href="getAuthRequest.php" id="btn-go" class="btn btn-primary btn-lg">Go!</a>

        <hr />
        <footer>
            <p>&copy; 2017 - BrightSpark Payment Gateway</p>
        </footer>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="scripts/auth.js"></script>
</body>
</html>