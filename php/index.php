<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/site.css" />
</head>
<body>
    <div class="container body-content">
            
        <div>
            <a href="index.php">Cleanup tool</a> | 
            <a href="whoAmI.php">Who am i?</a>
        </div>

        <div class="jumbotron">
            <h1>Simple bank account management tool</h1>
            <p class="lead">Free up some space on your account!</p>
        </div>

        <h2>1. In which bank your quota is full?</h2>

        <ul class="banks">
            <li data-provider="1">Swedbank</li>
            <li data-provider="2">SEB</li>
            <li data-provider="7">LHV</li>
            <li data-provider="6">Coop Pank</li>
            <li data-provider="9">Luminor</li>
        </ul>

        <h2>2. How much free space do you need?</h2>

        <div class="row amounts">
            <div class="col-md-4">
                <h2 data-amount="0.1">
                    0.1 €
                </h2>
            </div>
            <div class="col-md-4">
                <h2 data-amount="1">
                    1 €
                </h2>
            </div>
            <div class="col-md-4">
                <h2 data-amount="10">
                    10 €
                </h2>
            </div>
        </div>

        <a href="getPaymentRequest.php" id="btn-clean20" class="btn btn-primary btn-lg">Clean!</a>

        <hr />
        <footer>
            <p>&copy; 2017 - BrightSpark Payment Gateway</p>
        </footer>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="scripts/pay.js"></script>
</body>
</html>