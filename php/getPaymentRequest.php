<?php
require("_inc.php");

$url = $gwUrl.$clientId."/Transactions";

$providerId = $_POST["providerId"];
$amount = $_POST["amount"];

$returnUrl = "$localRootUrl/paymentResult.php";
$comment = "Cleanup service";
$invoiceNr = "X";

$ch = curl_init();

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, 
    "provider=".urlencode($providerId)
    ."&amount=".urlencode($amount)
    ."&returnUrl=".urlencode($returnUrl)
    ."&comment=".urlencode($comment)
    ."&invoiceNr=".urldecode($invoiceNr)
);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$res = curl_exec($ch);
curl_close($ch);

if ($res === false) {
    echo (curl_error($ch)); 
}
else {
    header("content-type: application/json");
    echo($res);
}

?>

