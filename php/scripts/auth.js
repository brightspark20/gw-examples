$(function() {
    
    var providerId;

    $(".banks li").click(function() {

        var $this = $(this);
        $this.siblings().removeClass("selected");
        $this.addClass("selected");

        providerId = $this.data("provider");
    });

    $("#btn-go").click(function (e) {

        e.preventDefault();

        if (!providerId) {
            alert("Bank not selected");
            return false;
        }

        var data = {
            providerId: providerId,
            clientId: $("#ClientId").val(),
            salt: $("#Salt").val(),
        };

        $.post($(this).attr("href"), data, function(res) {
            console.log(res);

            var $bankLink = $("<form style='display: none;' />");
            $bankLink.attr({ action: res.Url, method: res.Method || "post" });

            for (var i in res.Data) {
                if (res.Data[i] != null) {
                    $bankLink.append($("<div><span>" + i + "</span><input type=\"text\" name=\"" + i + "\" value=\"" + res.Data[i] + "\" /></div>"));
                }
            }

            $bankLink.append("<button type='submit'>Submit</button>");

            $bankLink.appendTo("body");
            $bankLink.submit();
        });
        return false;
    });

});