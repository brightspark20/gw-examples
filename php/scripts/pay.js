$(function() {

    var providerId;
    var amount;

    $(".banks li").click(function() {

        var $this = $(this);
        $this.siblings().removeClass("selected");
        $this.addClass("selected");

        providerId = $this.data("provider");
    });

    var $amounts = $(".amounts h2");
    $amounts.click(function () {

        var $this = $(this);
        $amounts.removeClass("selected");
        $this.addClass("selected");

        amount = $this.data("amount");
    });

    $("#btn-clean20").click(function (e) {

        e.preventDefault();

        if (!providerId) {
            alert("Bank not selected");
            return false;
        }

        if (!amount) {
            alert("Required free space not selected");
            return false;
        }

        var data = {
            providerId: providerId,
            amount: amount
        };

        $.post($(this).attr("href"), data, function (res) {
            var $form = $("<form style='display: none;' method='post' action='" + res.Url + "' />")
                .appendTo("body");

            for (var i in res.Data) {
                if (!res.Data.hasOwnProperty(i)) {
                    continue;
                }

                $form.append("<input type='hidden' name='" + i + "' value='" + res.Data[i] + "' />");
            }

            $form.submit();
        });

        return false;
    });


    $("#btn-clean21").click(function (e) {

        e.preventDefault();

        if (!providerId) {
            alert("Bank not selected");
            return false;
        }

        if (!amount) {
            alert("Required free space not selected");
            return false;
        }

        var data = {
            providerId: providerId,
            amount: amount
        };

        $.post($(this).attr("href"), data, function (res) {
            document.location.href = res;
        });

        return false;
    });

});