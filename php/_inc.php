<?php

$clientId = "your-client-id";
$clientSalt = "your-client-salt";

$host = "http://localhost";
$localRootUrl = "$host/gw-php-example";
$gwUrl = "https://gateway.brightspark.ee/api/v2/";

$returnUrl = "$localRootUrl/authResult.php";
$responseParamsPrefix = "x.";

function getNormalizedUrl($url, $prefix) {

    $idKey = $prefix . "id";
    $indexOfId = strpos($url, "?$idKey=");
    if ($indexOfId === false) {
        $indexOfId = strpos($url, "&$idKey=");
    }

    if ($indexOfId === false) {
        return false;
    }

    $params = ["id", "dateTime", "errCode", "authenticated", "userName", "firstName", "lastName", "userId", "country", "otherInfo", "token"];
    foreach($params as &$param) {
        $key = $prefix . $param;
        $normalizedKey = str_replace(".", "_", $key);
        $param = "$key=" . urlencode($_REQUEST[$normalizedKey]);
    }

    $normalizedUrl = substr($url, 0, $indexOfId + 1) . implode("&", $params);
    return strtolower($normalizedUrl);
}

function isValidUrl($url, $salt, $reqHash, $reqDateTime, $prefix) {

    $normalizedUrl = getNormalizedUrl($url, $prefix);
    if ($normalizedUrl === false) {
        return false;
    }

    $hashInput = $normalizedUrl . $salt;
    $hash = sha1($hashInput);
    
    $isValid = $hash == $reqHash;
    $fiveMinutesAgo = new DateTime();
    $fiveMinutesAgo->modify('-5 minutes');

    if ($isValid && $reqDateTime < $fiveMinutesAgo)
    {
        throw new Exception("Response has expired");
    }

    return $isValid;
}

function isValidUrlAuto() {

    $url = $GLOBALS["host"] . $_SERVER["REQUEST_URI"];
    $salt = $GLOBALS["clientSalt"];
    $prefix = $GLOBALS["responseParamsPrefix"];
    $reqHash = $_REQUEST["hash"];
    $reqDateTime = $_REQUEST["dateTime"];
    
    $dt = new DateTime($reqDateTime);
    return isValidUrl($url, $salt, $reqHash, $dt, $prefix);
}

?>